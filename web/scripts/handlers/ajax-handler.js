(function() {
    window.AjaxHandler = {};
    AjaxHandler = window.AjaxHandler;

    AjaxHandler.postJson = function(url, data, successCallback, errorCallback) {
        $.ajax({
            type: 'POST',
            url: url,
            contentType: 'application/json',
            data: JSON.stringify(data),
            dataType: 'json',
            success: successCallback,
            error: errorCallback
        });
    };

    AjaxHandler.get = function(url, successCallback, errorCallback) {
        $.ajax({
            type: 'GET',
            url: url,
            success: successCallback,
            error: errorCallback
        });
    };
})();