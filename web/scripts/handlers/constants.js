(function(){
    window.Constants = {};
    var Constants = window.Constants;
    Constants.Styles = {};
    Constants.Styles.Input = {
        Default: "default",
        Success: "success",
        Error: "error"
    };

    Constants.Paths = {};
    Constants.Paths.UsersUrl = "http://localhost:3000/api/users";
    Constants.Paths.LoginUrl = function(username, password) {
        var urlBase = "http://localhost:3000/api/login?username={0}&password={1}";
        var result = urlBase.replace('{0}', username)
                            .replace('{1}', password);
        return result;
    };
})();

