(function() {
    window.LocalStorageUtils = {};
    var LocalStorageUtils = window.LocalStorageUtils;

    LocalStorageUtils.save = function(key, value) {
        if (typeof(Storage) !== "undefined") {
            localStorage.setItem(key, value);
        } else {
            // Sorry! No Web Storage support..
        }
    }

    LocalStorageUtils.get = function(key) {
        if (typeof(Storage) !== "undefined") {
            localStorage.getItem(key);
        } else {
            // Sorry! No Web Storage support..
        }
    }

    LocalStorageUtils.remove = function(key) {
        if (typeof(Storage) !== "undefined") {
            localStorage.removeItem(key);
        } else {
            // Sorry! No Web Storage support..
        }
    }
})();