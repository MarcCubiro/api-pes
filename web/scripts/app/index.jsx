import React from 'react';
import ReactDOM from 'react-dom';
import Register from './register/register.jsx';
import Home from './home/home.jsx'

var register_container = document.getElementById("register-container");
if (register_container) {
    ReactDOM.render(
        <Register />,
        register_container
    );
}

var home_container = document.getElementById("home-container");
if (home_container) {
    ReactDOM.render(
        <Home />,
        home_container
    );
}