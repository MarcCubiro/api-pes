import React from 'react';
import RegisterInput from './register-input.jsx';

class RegisterForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      emailInput: {
        id: "emailInput",
        status: "",
        type: "email",
        translations: {
          title: "Email address",
          placeholder: "Enter email",
          message: (<span>We will not share your email with any third-party</span>)
        }
      },
      passwordInput: {
        id: "passwordInput",
        status: "",
        type: "password",
        translations: {
          title: "Password",
          placeholder: "Password",
          message: (<span></span>)
        }
      },
      passwordRepeatInput: {
        id: "passwordRepeatInput",
        status: "",
        type: "password",
        translations: {
          title: "Repeat password",
          placeholder: "Password",
          message: (<span></span>)
        }
      }
    }

    this.onEmailInputChange = this.onEmailInputChange.bind(this);
    this.onPasswordInputChange = this.onPasswordInputChange.bind(this);
    this.onPasswordRepeatInputChange = this.onPasswordRepeatInputChange.bind(this);
    this.onSubmitClick = this.onSubmitClick.bind(this);

    this.emailValue = "";
    this.passwordValue = "";
  }
  onEmailInputChange(e) {
    this.emailValue = e.target.value;
    var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i;

    var emailInput = this.state.emailInput;
    if (emailRegex.test(this.emailValue)) {
      emailInput.status = Constants.Styles.Input.Success;
      emailInput.translations.message = (<span className="success"><i className="fa fa-check"></i> The email is correct</span>);
    } else {
      emailInput.status = Constants.Styles.Input.Error;
      emailInput.translations.message = (<span className="error"><i className="fa fa-times"></i> Please, enter a valid mail</span>);
    }

    this.setState({
      emailInput: emailInput
    });
  }
  onPasswordInputChange(e) {
    this.passwordValue = e.target.value;
    
    var passwordInput = this.state.passwordInput;
    if (this.passwordValue.length >= 4) {
      passwordInput.status = Constants.Styles.Input.Success;
      passwordInput.translations.message = (<span className="success"><i className="fa fa-check"></i> Password length is correct</span>);
    } else {
      passwordInput.status = Constants.Styles.Input.Error;
      passwordInput.translations.message = (<span className="error"><i className="fa fa-times"></i> Password should have at least 4 characters</span>);
    }

    this.setState({
      passwordInput: passwordInput
    });
  }
  onPasswordRepeatInputChange(e) {
    var passwordRepeatValue = e.target.value;
    
    var passwordRepeatInput = this.state.passwordRepeatInput;
    if (passwordRepeatValue === this.passwordValue) {
      passwordRepeatInput.status = Constants.Styles.Input.Success;
      passwordRepeatInput.translations.message = (<span className="success"><i className="fa fa-check"></i> Passwords match</span>);
    } else {
      passwordRepeatInput.status = Constants.Styles.Input.Error;
      passwordRepeatInput.translations.message = (<span className="error"><i className="fa fa-times"></i> Passwords don't match</span>);
    }

    this.setState({
      passwordRepeatInput: passwordRepeatInput
    });
  }
  onSubmitClick(e) {
    e.preventDefault();

    var userRequest = {
      username: this.emailValue,
      email: this.emailValue,
      password: this.passwordValue
    }

    AjaxHandler.postJson(Constants.Paths.UsersUrl, userRequest,
      function(msg) {
        Cookies.set('loggedUser', userRequest);
        window.location = "/";
      },
      function() {
        // error
      }
    );
  }
  render() {
    return (
      <form className="register-form-component" autoComplete="off">
        <RegisterInput value={this.state.emailInput} onInputChange={this.onEmailInputChange} />
        <RegisterInput value={this.state.passwordInput} onInputChange={this.onPasswordInputChange} />
        <RegisterInput value={this.state.passwordRepeatInput} onInputChange={this.onPasswordRepeatInputChange} />
        <button className="btn btn-success pull-right" onClick={this.onSubmitClick}>Sign up</button>
      </form>
    );
  }
}

export default RegisterForm;