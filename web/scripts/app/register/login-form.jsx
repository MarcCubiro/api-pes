import React from 'react';

class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};

        this.onSignInClick = this.onSignInClick.bind(this);
    };
    onSignInClick(e) {
        e.preventDefault();

        var usernameValue = this.refs.username.value;
        var passwordValue = this.refs.password.value;

        AjaxHandler.get(Constants.Paths.LoginUrl(usernameValue, passwordValue),
            function(data) {
                Cookies.set('loggedUser', data);
                window.location = "/";
            },
            function(message) {
                console.log(message);
            }
        );
    };
	render() {
		return (
            <form className="pull-right form-inline">
                <input ref="username" className="form-control mr-sm-2" type="text" placeholder="Username or email" />
                <input ref="password" className="form-control mr-sm-2" type="password" placeholder="Password" />
                <button className="btn btn-success my-2 my-sm-0" onClick={this.onSignInClick}>Sign in</button>
            </form>
        );
    };
};

export default LoginForm;