import React from 'react';
import LoginForm from './login-form.jsx';
import RegisterForm from './register-form.jsx';

class Register extends React.Component {
	render() {
		return (
			<div className="app-container">
        		<nav className="register-top-menu navbar navbar-toggleable-md navbar-light bg-faded">
					<button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
							data-target="#registerTopMenuNavbar" aria-controls="registerTopMenuNavbar" aria-expanded="false"
							aria-label="Toggle navigation">
					<span className="navbar-toggler-icon"></span>
					</button>
					<a className="navbar-brand" href="#">ecoBoost</a>

					<div className="collapse navbar-collapse" id="registerTopMenuNavbar">
						<ul className="navbar-nav mr-auto">
							<li className="nav-item"><a className="nav-link" href="#">API Explorer</a></li>
							<li className="nav-item"><a className="nav-link" href="#">Docs</a></li>
							<li className="nav-item"><a className="nav-link" href="#">Help</a></li>
						</ul>
						<LoginForm />
					</div>
				</nav>
				<div className="app-jumbotron jumbotron">
					<div className="container">
						<div className="row">
							<div className="col-md-6">
								<h1>Welcome to ecoBoost!</h1>
								<p>Share the world with your friends, and promote ecologism</p>
							</div>
							<div className="col-md-6">
								<RegisterForm />
							</div>
						</div>
					</div>
				</div>
				<footer className="container-fluid">
					<div className="row">
						<div className="col-md-4">
							<p>Copyright Pau Torrents - 2017</p>
						</div>
						<div className="col-md-4">
							<p>Copyright Pau Torrents - 2017</p>
						</div>
						<div className="col-md-4">
							<p>Copyright Pau Torrents - 2017</p>
						</div>
					</div>
				</footer>
			</div>
		);
	};
};

export default Register;