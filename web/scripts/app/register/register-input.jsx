import React from 'react';

class RegisterInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      styles: {
        container: "",
        title: ""
      }
    };

    this.getStyles = this.getStyles.bind(this);
  }
  getStyles() {
    var styles = {
      container: "",
      title: ""
    }
    if (this.props.value.status === Constants.Styles.Input.Success) {
      styles = {
        container: "has-success",
        title: "success"
      };
    } else if (this.props.value.status === Constants.Styles.Input.Error) {
      styles = {
        container: "has-error",
        title: "error"
      };
    }

    return styles;
  }
  render() {
    var styles = this.getStyles();
    var containerClass = "register-input-component form-group " + styles.container;
    return(
      <div className={containerClass}>
        <label className={styles.title} htmlFor="emailInput">{this.props.value.translations.title}</label>
        <input ref="input" type={this.props.value.type} className="form-control"
              id={this.props.value.id} aria-describedby="help"
              placeholder={this.props.value.translations.placeholder}
              onChange={this.props.onInputChange}/>
        <small className="form-text text-muted">{this.props.value.translations.message}</small>
      </div>
    );
  }
}

export default RegisterInput;