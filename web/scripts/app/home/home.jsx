import React from 'react';
import FixedAlert from '../shared/fixed-alert.jsx';
import TopMenu from '../shared/top-menu.jsx';

class Home extends React.Component {
	render() {
		return (
			<div className="app-container">
				<FixedAlert />
        		<TopMenu />
			</div>
		);
	};
};

export default Home;