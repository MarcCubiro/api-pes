import React from 'react';

class TopMenu extends React.Component {
    constructor(props) {
        super(props);
        this.onLogoutClicked = this.onLogoutClicked.bind(this);
        this.onSettingsClicked = this.onSettingsClicked.bind(this);
    }
    onLogoutClicked() {
        Cookies.remove('loggedUser');
        window.location.reload();
    }
    onSettingsClicked() {
        
    }
    render() {
        var userJson = Cookies.get('loggedUser');
        var user = JSON.parse(userJson);
		return (
            <nav className="top-menu-container navbar navbar-toggleable-md navbar-light bg-faded">
                <div className="container full-height">
                    <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                        data-target="#topMenuNavbar" aria-controls="topMenuNavbar" aria-expanded="false"
                        aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <a className="navbar-brand" href="#">ecoBoost</a>

                    <div className="collapse navbar-collapse" id="topMenuNavbar">
                        <form className="top-menu-search-form form-inline mr-auto" autoComplete="off">
                            <div className="input-group">
                                <input ref="search-box" className="form-control mr-sm-2" type="text" placeholder="Search" aria-describedby="search-addon" />
                                <span className="input-group-addon my-2 my-sm-0" id="search-addon"><i className="fa fa-search"></i></span>
                            </div>
                        </form>
                        <ul className="navbar-nav pull-right top-menu-right-container">
                            <li className="nav-item">
                                <a className="nav-link">Hello {user.username}!</a>
                            </li>
                            <li className="nav-item dropdown">
                                <a className="nav-link nav-link-icon" href="http://example.com"
                                    id="optionsMenuDropdown" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false"><i className="fa fa-chevron-down"></i></a>
                                <div className="dropdown-menu dropdown-menu-right" aria-labelledby="optionsMenuDropdown">
                                    <a className="dropdown-item" onClick={this.onSettingsClicked} href="javascript:;">Settings</a>
                                    <a className="dropdown-item" onClick={this.onLogoutClicked} href="javascript:;">Logout</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        );
    }
}

export default TopMenu;