import React from 'react';

class FixedAlert extends React.Component {
    render() {
        return (
            <div className="fixed-alert">
                <span className="fixed-alert-cross"><i className="fa fa-times"></i></span>
                <h1>Ooops!</h1>
                <p>This is an alert</p>
            </div>
        );
    }
}

export default FixedAlert;