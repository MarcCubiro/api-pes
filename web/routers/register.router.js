var express = require('express');
var mongoose = require('mongoose');
var fs = require('fs');
var path = require('path');
var viewsDir = path.dirname(require.main.filename) + "/web/views/";
var Mustache = require('mustache');
var Config = require('../../config/config');
config = new Config();

var router = module.exports = express.Router();

router.get('/', function(req, res, next) {
  fs.readFile(viewsDir + 'register.mustache', function(err, template) {
    if (err) {
      throw err;
    }
    var json = {
      staticServer: config.STATIC_SERVER
    };
    var rendered = Mustache.render(template.toString(), {});
    res.header('Content-Type', 'text/html');
    res.status(200).send(rendered);
  });
});

router.all('*', function(req, res) { res.status(404).send("Recurso no encontrado"); });
