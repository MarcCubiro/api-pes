var express = require('express');
var mongoose = require('mongoose');
var async = require('async');
var bcrypt = require('bcrypt');
var request = require('request');
var utils = require('../utils/constants');
var Config = require('../../config/config');
config = new Config();
var router = module.exports = express.Router();

var User = mongoose.model('User');
var Post = mongoose.model('Post');
var Event = mongoose.model('Event');
var Place = mongoose.model('Place');

const usersNames = ["marc", "dani", "pau", "alex", "pepe", "xavi", "juanito", "pedro", "rafa", "jeison"];
const users = [];
const ipsum = [
    "Nullam ligula dolor, aliquet et pharetra eu, sollicitudin quis odio. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer rutrum arcu nec lorem vulputate, et gravida diam aliquet. Phasellus consequat vehicula sollicitudin. Etiam a nisl non lacus bibendum luctus id vel est. Fusce maximus, lacus a scelerisque volutpat, metus dolor eleifend nunc, non eleifend arcu diam sed augue. Donec scelerisque risus risus, a semper libero tempor quis. Duis rutrum aliquam orci ac luctus.",
    "Curabitur egestas massa nec nibh placerat aliquam. Cras ultricies lectus ut mauris interdum, nec faucibus tellus ullamcorper. Suspendisse potenti. Pellentesque eleifend vehicula metus non interdum. Proin faucibus nibh in finibus finibus. Vivamus in mattis urna. Pellentesque est massa, fringilla fringilla elit quis, lacinia euismod magna.",
    "Phasellus et arcu et purus aliquet faucibus eget non arcu. Vestibulum ultricies eu magna eu dignissim. Sed bibendum sem magna, non bibendum nisl finibus id. Phasellus nibh dui, porta lacinia lacus at, tristique facilisis libero. Sed vulputate vitae tellus et pulvinar. Nam cursus magna in pharetra facilisis. Maecenas metus nunc, bibendum ac nisi vel, tempus maximus odio. Duis non enim venenatis, aliquet leo sed, laoreet orci. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a tortor urna. Morbi feugiat cursus libero, sit amet maximus sem efficitur vitae.",
    "Phasellus lacinia tristique scelerisque. Cras tempor fermentum turpis sit amet commodo. Curabitur mollis orci in sapien fringilla, a venenatis nibh pretium. Integer id varius tortor. Nulla tristique condimentum blandit. Nunc et turpis eget erat porta tincidunt. Pellentesque dolor nunc, interdum ac imperdiet a, eleifend tristique velit. Maecenas dapibus scelerisque sem eget accumsan. Nullam maximus id sem at interdum. Fusce eu arcu libero. Donec sit amet convallis felis. Cras dapibus rhoncus leo, pharetra gravida ligula condimentum sed. Duis egestas sodales metus at cursus. Nullam sit amet luctus nisi. In vulputate eros diam, sit amet efficitur ante mollis efficitur.",
    "Morbi eu aliquet urna. Curabitur mattis porta ligula bibendum pretium. Vivamus semper erat et tempor sollicitudin. Quisque rutrum, velit id dapibus mattis, libero metus tincidunt eros, nec blandit lacus felis gravida risus. Nulla tempus at risus at auctor. Etiam at dolor cursus, placerat est ac, ornare nisi. Phasellus eget dapibus mi, nec interdum mauris. Nunc dignissim cursus turpis ut tincidunt. Suspendisse id augue eros. Pellentesque nec purus lorem. Donec neque erat, dignissim in laoreet sed, porta nec ipsum.",
    "Proin vel enim euismod, malesuada nulla et, varius velit. Etiam sit amet lorem egestas augue sodales accumsan nec at ante. Phasellus arcu purus, pulvinar sed efficitur nec, mattis non ipsum. Aenean gravida lacus ut semper ornare. Etiam cursus faucibus ante, et rhoncus turpis laoreet ut. Phasellus sed ante porta, placerat purus vestibulum, sagittis odio. Suspendisse suscipit, eros eget vulputate vulputate, massa sem maximus risus, eu posuere diam nunc id libero.",
    "Sed nisl velit, placerat ut nibh sed, mattis volutpat quam. Duis hendrerit finibus diam, vulputate volutpat velit vestibulum ut. Nam convallis vel turpis egestas suscipit. Quisque at ante varius, iaculis enim id, rhoncus urna. Proin egestas ornare metus, vel ultrices lacus lobortis vel. Nunc posuere varius nunc et vulputate. Nullam at porta augue. Praesent est ante, ullamcorper et magna in, tincidunt tempus nisi. Donec eleifend ipsum egestas ante fringilla, eu viverra turpis porttitor.",
    "Aenean consectetur, ex mollis posuere consectetur, nibh ipsum fringilla magna, eu ornare magna nulla eget arcu. Donec in dolor luctus nulla pretium consectetur. Proin consequat congue nisi, nec auctor tellus feugiat sed. Vivamus lacus odio, pharetra vitae viverra vel, cursus congue mi. Phasellus quis odio id eros ultricies porttitor non nec massa. Integer placerat dignissim varius. Duis lacinia est in nibh auctor sollicitudin. Donec volutpat quam non nunc luctus, quis lacinia massa ultrices. Nunc id consequat urna, non bibendum eros. Pellentesque in lacus odio. Nulla eu eros nec nunc sollicitudin imperdiet. Nam finibus ornare enim, vitae scelerisque ante bibendum congue. Pellentesque pharetra quam vitae rhoncus congue. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
    "Proin nec pellentesque lectus. Nunc dolor dui, ullamcorper ac urna at, scelerisque vehicula diam. Curabitur scelerisque ultrices nisl, non laoreet dui sodales eu. Duis faucibus dolor elit, sed ultrices ipsum fermentum non. Nunc non quam non lorem eleifend rhoncus sed id leo. Proin mattis, nunc eget iaculis dapibus, justo risus euismod dui, sit amet finibus felis erat vitae mauris. Quisque laoreet neque eros, non sodales augue accumsan quis. Duis in nunc in quam gravida scelerisque sed sit amet diam.",
    "Donec sem lectus, auctor sit amet augue at, lobortis tincidunt augue. Nulla vehicula molestie nunc quis mollis. Donec sit amet est ullamcorper, pretium odio ut, tempus massa. Duis sapien augue, placerat quis porttitor eu, semper quis ligula. Morbi ac dolor ac nibh iaculis tincidunt. Aliquam elementum massa erat. Proin eget iaculis urna.",
    "Proin euismod elit efficitur dapibus imperdiet. Nam elementum pulvinar eros, ut scelerisque velit pellentesque a. Ut viverra sem in suscipit dignissim. Pellentesque imperdiet, nulla eget laoreet egestas, nulla neque egestas diam, sit amet volutpat ex dolor id mi. Mauris bibendum nec justo ut mollis. Etiam scelerisque justo turpis, in semper leo euismod sed. Vestibulum sodales, lorem at rutrum luctus, ante nisl elementum ligula, eget tempor felis sem sed felis. Cras tristique interdum risus quis mattis. Maecenas vel dui mi. Vestibulum eu pharetra lorem. Vivamus quis faucibus lacus. Phasellus euismod, nulla ac lobortis ultricies, diam ante accumsan quam, ut ultrices arcu nisi a urna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam blandit augue a est auctor, et consectetur diam accumsan.",
    "Ut vel nisl urna. Maecenas ultrices molestie dolor, pellentesque vestibulum nunc viverra vel. Donec auctor feugiat dui, non luctus lorem tempus eu. Cras hendrerit tincidunt tellus ut placerat. Cras pretium neque mi, vitae sagittis dui sodales tempor. In lobortis quam eget lacus mattis, sed vulputate augue fringilla. Aliquam erat volutpat. Duis id dignissim libero. Vestibulum feugiat fringilla massa, sit amet euismod tellus semper semper.",
    "Sed orci odio, malesuada a egestas at, facilisis sed sapien. Quisque vel nisi ut diam posuere aliquet vel ut tellus. Morbi consectetur urna odio, sodales aliquet mi vestibulum nec. Integer mattis accumsan magna, in pellentesque augue volutpat in. Etiam ornare dui sed porta interdum. Suspendisse eget condimentum nunc, non cursus lectus. Maecenas in nulla at metus aliquet cursus eu eleifend massa. Curabitur consequat tortor felis, suscipit sagittis odio rhoncus a.",
    "Proin et dui vel purus placerat porttitor non sit amet nibh. Interdum et malesuada fames ac ante ipsum primis in faucibus. Morbi maximus gravida tellus. Donec enim quam, malesuada eu arcu nec, accumsan fermentum sem. Vestibulum egestas, dolor tristique feugiat gravida, leo tortor elementum ante, et ultricies tortor nibh ac massa. Etiam nec viverra orci, id suscipit massa. Aliquam eros sapien, congue ut mi maximus, vulputate molestie lacus. Proin id lacinia lacus. Ut eu blandit massa, rutrum mattis dolor.",
    "Nullam erat nulla, efficitur sit amet hendrerit vel, mollis ac nisi. Duis augue nibh, aliquam vitae tristique et, maximus ac quam. Morbi pulvinar quis risus sed accumsan. Sed ac egestas mauris, non sagittis mi. Maecenas congue tincidunt ultrices. Cras pretium arcu vitae justo lacinia, ut placerat est laoreet. Proin posuere, nisl nec varius lobortis, ex lacus cursus erat, ut tincidunt massa eros quis mauris. Maecenas a convallis ligula, vel venenatis dui. Donec tincidunt tincidunt quam, vel viverra neque aliquet ut. Fusce faucibus diam nunc. Maecenas lacinia ante condimentum ornare blandit. Donec finibus quam lectus, condimentum pulvinar urna bibendum vel. Vestibulum id augue tincidunt, feugiat sapien vel, bibendum est. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nulla facilisi. Proin id rutrum enim."
]

function getRandomIpsum() {
    var i = Math.floor(Math.random() * (ipsum.length-1));
    return ipsum[i];
}

String.prototype.capitalize = function () {
    return this.replace(/(?:^|\s)\S/g, function (a) {
        return a.toUpperCase();
    });
};

router.get('/', function (req, res) {
    removeAll(function () {
        insertUsers(usersNames, function () {
            insertPosts(users, function () {
                insertEvents(users, function () {
                    insertUrbanOrchard(function () {
                        res.status(200).json("ok");
                    });
                });
            });
        });
    });
});

function removeAll(callback) {
    User.remove({}, function (err) {
        Event.remove({}, function (err) {
            Place.remove({}, function (err) {
                return callback();
            })
        });
    });
}

function insertUsers(usersNames, callback) {
    usersNames.forEach(function (name, i) {
        var passwd = bcrypt.hashSync("1234", 12);
        var user = User({
            name: name.capitalize(),
            username: name,
            password: passwd,
            email: name + "@gmail.com"
        });
        user.save(function (err, usersaved) {
            users.push(usersaved)
            if (users.length === usersNames.length) return callback();
        });
    });
}

function insertPosts(users, callback) {
    users.forEach(function (user, i) {
        var rand = Math.floor(Math.random() * 7) + 1;
        for (var j = 0; j < rand; j++) {
            var post = Post({
                text: getRandomIpsum()
            });
            user.posts.push(post);
        }
        user.save();
        if (i == users.length - 1) return callback();
    });
}

function insertEvents(users, callback) {
    users.forEach(function (user, i) {
        var rand = Math.floor(Math.random() * 3) + 1;
        for (var j = 0; j < rand; j++) {
            var post = Post({
                text: getRandomIpsum()
            });
            user.posts.push(post);
            user.save(function (err, userSaved) {
                var post = userSaved.posts[userSaved.posts.length - 1];
                var event = Event({
                    userId: userSaved._id,
                    postId: post._id,
                    title: getRandomIpsum(),
                    location: {
                        type: "Point",
                        coordinates: [41.34 + Math.random() * 0.08, 2.05 + Math.random() * 0.08]
                    }
                });
                event.save();
                if (i == (users.length - 1) && j == rand) return callback();
            });
        }
    });
}

function insertUrbanOrchard(callback) {
    var orchards = utils.ORCHARDS_SEED;
    for (var i in orchards.names) {
        var place = Place({
            type: "orchard",
            orchardName: orchards.names[i],
            location: {
                type: "Point",
                coordinates: [parseFloat(orchards.latitudes[i]), parseFloat(orchards.longitudes[i])]
            }
        });
        place.save(function (err) {
            if (err) return callback(err);
            if (parseInt(i) == orchards.names.length - 1) return callback();
        });
    }
}