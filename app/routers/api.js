var express = require('express');
var routers = require('./index.js');                // get routers
var apiRouter = module.exports = express.Router();  // get all routers files

apiRouter.use('/login', routers.login);
apiRouter.use('/users', routers.user);
// Get user data for all /posts
apiRouter.use('/users/:userId', function(req, res, next) {
    res.locals.userId = req.params.userId;
    return next();
});
apiRouter.use('/users/:userId/posts', routers.post);
apiRouter.use('/events', routers.event);
apiRouter.use('/places', routers.places);
apiRouter.use('/seed', routers.seed);
apiRouter.use('/search', routers.search);
