var express = require('express');
var mongoose = require('mongoose');
var async = require('async');
var request = require('request');
var Config = require('../../config/config');
config = new Config();
var router = module.exports = express.Router();

var User = mongoose.model('User');

router.get('/', function (req, res) {
    var query = ".*"+req.query.query+".*";
    var searchQuery = { $or: [ { username: {'$regex': query, $options: 'i'} },
        { name: {'$regex': query, $options: 'i'} },
        { lastname: {'$regex': query, $options: 'i'} }
    ]
    };
    User.find(searchQuery, function (err, users) {
        if (err) return res.status(404).json("User not found.");
        var result = [];
        users.forEach(function (user, i) {
            user.exportData(function (u) {
                result.push(u);
                if (i == (users.length - 1)) return res.status(200).json({users: result});
            });
        });
    });
});

