var express = require('express');
var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var async = require('async');
var express_jwt = require('express-jwt');
var Config = require('../../config/config');
config = new Config();
var constants = require('../utils/constants');
var User = mongoose.model('User');
var Post = mongoose.model('Post');
var Event = mongoose.model('Event');
var router = module.exports = express.Router();
var jwt = require('jsonwebtoken');
var eventService = require('../services/event.service.js');
var uploadService = require('../services/upload.service.js');
var url = require('url');
var path = require('path');


/* Get all users */
router.get('/', function (req, res) {
    User.find({}, function (err, users) {
        if (err) return res.status(500).json(err);
        var result = [];
        users.forEach(function (user, i) {
            user.exportData(function (u) {
                result.push(u);
                if (i == (users.length - 1)) return res.status(200).json({users: result});
            });
        });
    });
});

/* RANKING */
router.get('/ranking', function (req, res) {
    User.find({}).limit(5).sort({points: -1}).exec(function (err, users) {
        if (err) return res.status(500).json(err);
        var result = [];
        users.forEach(function (user, i) {
            user.exportData(function (u) {
                result.push(u);
                if (i === (users.length - 1)) return res.status(200).json({users: result});
            });
        });
    });
});

/* Register: email and password and username fields are required other fields are optional */
router.post('/', function (req, res) {
    // check required fields
    if (!req.body.email || !req.body.password || !req.body.username) {
        return res.status(400).json({"status": "Username, email or password are empty"});
    }
    bcrypt.hash(req.body.password, 12, function (err, hashedPassword) {
        if (err) return res.status(500).json({status: "error", message: err.message});
        req.body.password = hashedPassword;
        req.body.points = constants.REGISTER_POINTS;
        var user_instance = new User(req.body);
        user_instance.save(function (err) {
            if (err) return res.status(500).json({status: "error", message: err.message});
            user_instance.exportData(function (u) {
                var response = {
                    token: jwt.sign({id: user_instance._id, username: req.body.username}, config.JWT_SECRET),
                    user: u
                };
                res.status(201).json(response);
            });
        });
    });
});

router.use(express_jwt({secret: config.JWT_SECRET}));

/* Get pofile info */
router.get('/:id', function (req, res) {
    User.findOne({_id: req.params.id}, function (err, user) {
        if (err) return res.status(404).json("User not found.");
        user.exportData(function (u) {
            res.status(200).json(u);
        });
    });
});

/* Verify user middleware*/
router.use('/:id', function (req, res, next) {
    /*req.user es la informacio del token desencriptat*/
    if (req.user.id === req.params.id) next();
    else res.status(401).json({status: false, message: "Unauthorized"});
});

/* Edit pofile info */
router.put('/:id', function (req, res) {
    if (req.body.password) {
        bcrypt.hash(req.body.password, 12, function (err, hashedPassword) {
            if (err) return res.status(500).json({status: "error", message: err.message});
            req.body.password = hashedPassword;
        });
    }
    User.findByIdAndUpdate(req.params.id, {$set: req.body}, function (err, user) {
        if (err) return res.status(400).json({status: "error", message: err.message});
        user.exportData(function (u) {
            res.status(200).send({status: "success", result: u});
        });
    })
});

router.post('/:id/image', function (req, res) {
    uploadService.getFile(req, 'users', function (code, msg, file, serverPath, staticPath) {
        if (code) {
            return res.status(code).send(msg);
        }
        // Use the mv() method to place the file somewhere on your server
        file.mv(serverPath, function (err) {
            if (err)
                return res.status(500).send(err);
            User.findByIdAndUpdate(req.params.id, {$set: {imageUri: staticPath}}, {new: true}, function (err, user) {
                if (err) return res.status(400).json({status: "error", message: err.message});
                user.exportData(function (u) {
                    res.status(200).send({status: "success", result: u});
                });
            });
        });
    });
});

// Query params: userTofollowId
router.post('/:id/follow', function (req, res) {
    User.findOne({_id: req.params.id}, function (err, user) {
        if (err) return res.status(500).json("Internal error");
        if (!user) return res.status(404).json("User not found");
        User.findOne({_id: req.body.userTofollowId}, function (err, userTofollow) {
            if (err) return res.status(500).json("Internal error");
            if (!userTofollow) return res.status(404).json("User to follow not found");
            user.following.push(req.body.userTofollowId);
            userTofollow.followers.push(req.params.id);
            user.save();
            userTofollow.save();
            res.status(200).json("done");
        })
    });
});

// Query params: userToUnfollowId
router.post('/:id/unfollow', function (req, res) {
    User.findOne({_id: req.params.id}, function (err, user) {
        if (err) return res.status(500).json("Internal error");
        if (!user) return res.status(404).json("User not found");
        User.findOne({_id: req.body.userToUnfollowId}, function (err, userToUnfollow) {
            if (err) return res.status(500).json("Internal error");
            if (!userToUnfollow) return res.status(404).json("User to follow not found");
            var index = user.following.indexOf(req.body.userToUnfollowId);
            if (index > -1) {
                user.following.splice(index, 1);
            }
            index = userToUnfollow.followers.indexOf(req.params.id);
            if (index > -1) {
                userToUnfollow.followers.splice(index, 1);
            }
            user.save();
            userToUnfollow.save();
            res.status(200).json("done");
        })
    });
});

/* Delete user */
router.delete('/:id', function (req, res) {
    User.remove({_id: req.params.id}, function (err) {
        if (err) return res.status(500).json(err);
        else res.status(200).send({status: "user deleted"});
    });
});

/* Get following users posts
 * Parameters:
 *   n  = how many posts
 *   first = position of first post
 *   nEvents = number of randomized events
 * */
router.get('/:id/feed', function (req, res, next) {
    var first = 0;
    var n = 50;
    var nEvents = 4;
    var userFeedID = req.params.id;
    if (req.query.first !== undefined) first = parseInt(req.query.first);
    if (req.query.n !== undefined) n = parseInt(req.query.n);
    User.findOne({_id: req.params.id}, function (err, user) {
        if (err) return res.status(404).json("User not found");
        // execute all mongodb calls in parallel
        var feed = user.posts;
        async.each(user.following, function (userId, callback) {
            User.findOne({_id: userId}, function (err, followedUser) {
                feed.push.apply(feed, followedUser.posts.slice(0, n));
                callback();
            }).sort({date: -1})
        }, function (err) {
            if (err) return next(err);
            var feedSortedByDate = feed.sort(function (o1, o2) {
                return o2.date - o1.date
            });
            feedSortedByDate = feedSortedByDate.slice(first, first + n);
            if (req.query.latitude !== undefined && req.query.longitude !== undefined) {
                var geoJSONPoint = {
                    type: "Point",
                    coordinates: [
                        parseFloat(req.query.longitude),
                        parseFloat(req.query.latitude)
                    ]
                };
                eventService.getNearEvents(constants.FEED_MIN_DISTANCE, constants.FEED_MAX_DISTANCE, constants.FEED_MAX_EVENTS, geoJSONPoint, function (err, result) {
                    if (err) return res.status(500).json(err);
                    for (var i = 0; i < nEvents && i < result.length; i++) {
                        var tmpEvent = Event(result[i]);
                        tmpEvent.exportData(function (e) {
                            var random = Math.floor(Math.random() * n - 1);
                            feedSortedByDate.splice(random, 0, e);
                        });
                    }
                    //return res.status(200).json(feedSortedByDate);

                    var result = [];
                    if (feedSortedByDate.length === 0) return res.status(200).json({feed: result});
                    feedSortedByDate.forEach(function (post, i) {
                        if(post.userId !== undefined){
                            var tmpEvent = new Event(post);
                            tmpEvent.exportDataToPost(post.userId, post.postId, function (post) {
                                result.push(post);
                                if (i === feedSortedByDate.length - 1) return res.status(200).json({posts: result});
                            })
                        }
                        else {
                            var tmpPost = new Post(post);
                            tmpPost.exportData(userFeedID, function (p) {
                                result.push(p);
                                if (i === feedSortedByDate.length - 1) return res.status(200).json({posts: result});
                            })
                        }
                    });
                });
            }
            else {
                var result = [];
                if (feedSortedByDate.length === 0) return res.status(200).json({posts: result});
                feedSortedByDate.forEach(function (post, i) {
                    var tmpPost = new Post(post);
                    tmpPost.exportData(userFeedID, function (p) {
                        result.push(p);
                        //TODO: A vegades no retorna tots els posts, nose perque?
                        if (i === feedSortedByDate.length - 1) return res.status(200).json({posts: result});
                    })
                });
            }
        });
    });
});

//router.all('*', function(req, res) { res.status(404).send("Recurso no encontrado"); });
