var express = require('express');
var mongoose = require('mongoose');
var async = require('async');
var request = require('request');
var utils = require('../utils/constants');
var Config = require('../../config/config');
config = new Config();
var express_jwt = require('express-jwt');

var Place = mongoose.model('Place');
var router = module.exports = express.Router();

router.use(express_jwt({secret: config.JWT_SECRET}));

/* Return places by proximity */
router.get('/near/:longitude/:latitude', function(req, res) {
    var minDistance = 0;
    var maxDistance = 5000; // 5km
    var num = 100;
    if(req.query.minDistance !== undefined) minDistance = parseFloat(req.query.minDistance);
    if(req.query.maxDistance !== undefined) maxDistance = parseFloat(req.query.maxDistance);
    if(req.query.num !== undefined) num = parseInt(req.query.num);
    updateBicingStations(function (err) {
        if(!err){
            var geoJSONPoint = {
                type: "Point",
                coordinates: [
                    parseFloat(req.params.latitude),
                    parseFloat(req.params.longitude)
                ]
            };
            Place.aggregate([
                {
                    $geoNear: {
                        near: geoJSONPoint,
                        distanceField: "distance",
                        minDistance: minDistance,
                        maxDistance: maxDistance,
                        num: num,
                        spherical: true
                    }
                }
            ], function (err, result) {
                if(err) return res.status(500).json(err);
                return res.status(200).json({places: result});
            });
        }
    });
});

router.put('/force/update', function(req, res) {
    insertUrbanOrchard(function (err) {
        if(err) return res.status(500).json({result: err});
        updateBicingStations(function (err) {
            if(err) return res.status(500).json({result: err});
            res.status(200).json({result: "updated"});
        });
    });
});

router.post('/', function(req, res) {
    var place = Place(req.body);
    place.save(function (err, place) {
        if(err) return res.status(500).json({result: err});
        res.status(201).json({place: place});
    });
});

function updateBicingStations(callback) {
    request(utils.BICING_URL, function (err, response, body) {
        if(err) return console.log("Error while bicing update");
        body = JSON.parse(body);
        var stations = body.stations;
        for (var i in stations) {
            Place.update({bikeStationId: stations[i].id},{ $set: {
                type: "bicingStation",
                bikeStationId: stations[i].id,
                availableBikes: parseInt(stations[i].bikes),
                location: {
                    type: "Point",
                    coordinates: [parseFloat(stations[i].latitude), parseFloat(stations[i].longitude)]
                },
                orchardName: ""
            }}, {upsert: true}, function (err, res) {
                if (err) return callback(err);
            })
        }
        return callback();
    })
}