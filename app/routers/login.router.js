var express = require('express');
var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');

var router = module.exports = express.Router();
var User = mongoose.model('User');

/* LOGIN */
router.post('/', function(req, res) {
    if(!req.body.username) res.status(401).send({msg: "Username is required"});
    if(!req.body.password) res.status(401).send({msg: "Password is required"});
    else{
        User.findOne({username: req.body.username}, function (err, user) {
            if(err) res.status(500).send({msg:"Unexpected error"});
            if(!user) res.status(404).send({msg:"Bad username/password"});
            else{
                user.comparePassword(req.body.password, function (err, isMatch) {
                    if(err) res.status(500).send({msg:"Unexpected error"});
                    if(!isMatch) res.status(401).send({msg: "Bad username/password"});
                    else{
                        var token = jwt.sign({id:user._id, username: req.body.username}, config.JWT_SECRET);
                        //res.status(200).json({token : token, user: user});// TODO cambiar user per u
                        user.exportData(function (u) {
                            res.status(200).json({token : token, user: u});// TODO cambiar user per u
                        });

                    }
                });
            }
        });
    }
});

router.all('*', function(req, res) { res.status(404).send("Resource not found"); });