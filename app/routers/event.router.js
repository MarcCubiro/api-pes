var express = require('express');
var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var async = require('async');
var request = require('request');
var utils = require('../utils/constants');
var Config = require('../../config/config');
config = new Config();
var express_jwt = require('express-jwt');
var postService = require('../services/post.service.js');
var eventService = require('../services/event.service.js');
var uploadService = require('../services/upload.service.js');

var Event = mongoose.model('Event');
var router = module.exports = express.Router();

router.get('/', function (req, res) {
    Event.find({}, function (err, events) {
        if (err) return res.status(500).send("Error retrieving events");
        var ev = [];
        if(events.length > 0) {
            events.forEach(function (event, i) {
                event.exportData(function (e) {
                    ev.push(e);
                    if (i == events.length - 1) {
                        return res.status(200).json({events: ev});
                    }
                })
            });
        }
        else {
            return res.status(200).json({events: events});
        }
    });
});

/* Find near event by location */
router.get('/near/:latitude/:longitude', function (req, res) {
    var minDistance = 0;
    var maxDistance = 5000; // 5km
    var num = 100;
    if (req.query.minDistance !== undefined) minDistance = parseFloat(req.query.minDistance);
    if (req.query.maxDistance !== undefined) maxDistance = parseFloat(req.query.maxDistance);
    if (req.query.num !== undefined) num = parseInt(req.query.num);
    var geoJSONPoint = {
        type: "Point",
        coordinates: [
            parseFloat(req.params.longitude),
            parseFloat(req.params.latitude)
        ]
    };
    eventService.getNearEvents(minDistance, maxDistance, num, geoJSONPoint, function (err, events) {
        if (err) return res.status(500).json(err);
        var ev = [];
        if(events.length > 0){
            events.forEach(function (event, i) {
                var tmpEvent = Event(event);
                tmpEvent.exportData(function (e) {
                    ev.push(e);
                    if(i == events.length -1){
                        return res.status(200).json({events: ev});
                    }
                })
            });
        }
        else{
            return res.status(200).json({events: events});
        }
    });
});
router.use(express_jwt({secret: config.JWT_SECRET}));
/* Verify user middleware*/

router.use('/:id', function (req, res, next) {
    //req.user es la informacio del token desencriptat
    if (req.user.id === req.body.userId) next();
    else res.status(401).json({status: false, message: "Unauthorized"});
});

/* Post event: Needs a property 'event' and a property 'post', as the two will be created */
router.post('/', function (req, res) {
    // check required fields
    if (!req.body.event || !req.body.event.userId || !req.body.event.title || !req.body.post || !req.body.event.location) {
        return res.status(400).json({"status": "Request is not valid (missing properties)"});
    }

    postService.post(req.body.post, function (err, post) {
        var eventRequest = req.body.event;
        eventRequest.postId = post._id;
        var event = new Event(eventRequest);
        event.save(function (err, eventSaved) {
            if (err) return res.status(500).json(err);
            post = post.toObject();
            post.eventId = eventSaved._id.toString();
            post.userId = eventSaved.userId.toString();
            postService.put(post, function (err) {
                if (err) return res.status(500).json(err);
                event.exportData(function (e) {
                    res.status(201).json({
                        event: e,
                        post: post
                    });
                })
            });
        });
    });
});

/* Edit event info */
router.put('/:id', function (req, res) {
    var eventId = req.params.id;
    var eventRequest = req.body;
    eventRequest._id = eventId;
    Event.findByIdAndUpdate(eventId, {$set: eventRequest}, {new: true}, function (err, event) {
        if (err) return res.status(400).json({status: "error", message: err.message});
        event.exportData(function (e) {
            res.status(200).json(e);
        })

    })
});

/* Delete event: Delete from events and user posts */
router.delete('/:id', function (req, res, next) {
    var eventId = req.params.id;
    Event.findOne({_id: eventId}, function (err, event) {
        if (err) return res.status(404).send("Event not found");
        Event.remove({_id: event.eventId}, function (err) {
            if (err) return res.status(500).json(err);
            postService.delete(event.userId, event.postId, function (err) {
                if (err) return res.status(404).send("Post for event not found");
                res.status(200).send("Event removed");
            });
        });
    });
});

/* Update event image */
router.post('/:id/image', function (req, res) {
    uploadService.getFile(req, 'events', function (code, msg, file, serverPath, staticPath) {
        if (code) {
            return res.status(code).send(msg);
        }
        // Use the mv() method to place the file somewhere on your server
        file.mv(serverPath, function (err) {
            if (err)
                return res.status(500).send(err);
            Event.findByIdAndUpdate(req.params.id, {$set: {imageUri: staticPath}}, {new: true}, function (err, event) {
                if (err) return res.status(400).json({status: "error", message: err.message});
                event.exportData(function (e) {
                    res.status(200).send({status: "success", result: e});
                });

            });
        });
    });
});

/* Enroll to event */
router.post('/:id/enroll', function (req, res) {
    // check required fields
    Event.findOne({_id: req.params.id}, function (err, event) {
        if (err) res.status(404).json("Event not found.");
        else {
            if (event.attenders.indexOf(req.body.userId) > -1) return res.status(400).json("Already enrolled");
            event.attenders.push(req.body.userId);
            event.save();
            return res.status(200).json("Enrolled");
        }
    });
});

/* No attend to event */
router.post('/:id/unattend', function (req, res) {
    // check required fields
    Event.findOne({_id: req.params.id}, function (err, event) {
        if (err) res.status(404).json("Event not found.");
        else {
            if (event.attenders.indexOf(req.body.userId) === -1) return res.status(400).json("Not enrolled");
            event.attenders.pop(req.body.userId);
            event.save();
            return res.status(200).json("Not attending");
        }
    });
});


/* Report event */
router.post('/:id/report', function (req, res) {
    // check required fields
    Event.findOne({_id: req.params.id}, function (err, event) {
        if (err) res.status(404).json("Event not found.");
        else {
            postService.report(event.userId, event.postId, req.body.userId, function (err, status, msg) {
                if (err) return res.status(500).json(err);
                if (status === 404) return res.status(404).send("Post not found");
                return res.status(status).json(msg);
            });
        }
    });
});


router.all('*', function (req, res) {
    res.status(404).send("Recurso no encontrado");
});
