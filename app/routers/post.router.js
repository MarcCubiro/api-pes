var express = require('express');
var mongoose = require('mongoose');
var config = require('../../config/config');
var postService = require('../services/post.service.js');
var userService = require('../services/user.service.js');
var uploadService = require('../services/upload.service.js');
var constants = require('../utils/constants');
var User = mongoose.model('User');
var Post = mongoose.model('Post');
var router = module.exports = express.Router();
var express_jwt = require('express-jwt');

/* Get all posts from user */
router.get('/', function (req, res, next) {
    var userId = res.locals.userId;
    User.findOne({_id: userId.toString()}, function (err, user) {
        if (err) return res.status(404).json(err);
        var posts = user.posts;
        posts.sort(comparePosts);
        var result = [];
        posts.forEach(function (post, i) {
            var tmpPost = new Post(post);
            tmpPost.exportData(userId, function (p) {
                result.push(p);
                if (i == (posts.length - 1)) return res.status(200).json({posts: result});
            });
        });
    });
});

/* Get post */
router.get('/:postId', function (req, res, next) {
    var userId = res.locals.userId;
    var postId = req.params.postId;
    postService.getById(userId, postId, function (err, status, post) {
        if (err) return res.status(500).json(err);
        if (status === 404) return res.status(404).json("Post not found");
        post.exportData(userId, function (p) {
            res.status(status).json(p);
        })
    });
});

/* Insert post */
router.post('/', function (req, res, next) {
    var userId = res.locals.userId;
    var postRequest = req.body;
    postRequest.userId = userId;
    postService.post(postRequest, function (err, newPost) {
        if (err) return res.status(500).json(err);
        userService.sumPoints(userId, constants.POST_POINTS, function (err, status, user) {
            if (err) return res.status(500).json(err);
            newPost.exportData(userId, function (p) {
                res.status(201).json(p);
            })

        });
    });
});

/* Update post */
router.put('/:postId', function (req, res, next) {
    var userId = res.locals.userId;
    var postId = req.params.postId;
    var postRequest = req.body;
    postRequest.userId = userId;
    postRequest._id = postId;
    postService.put(postRequest, function (err) {
        if (err) return res.status(500).json(err);
        return res.status(200).json("Post updated");
    });
});

/* Delete post */
router.delete('/:postId', function (req, res, next) {
    var userId = res.locals.userId;
    var postId = req.params.postId;
    postService.delete(userId, postId, function (err) {
        if (err) return res.status(404).send("Post not found");
        res.status(200).json("Post deleted");
    });
});

router.post('/:postId/image', function (req, res) {
    var userId = res.locals.userId;
    var postId = req.params.postId;
    uploadService.getFile(req, 'posts', function (code, msg, file, serverPath, staticPath) {
        if (code) {
            return res.status(code).send(msg);
        }
        // Use the mv() method to place the file somewhere on your server
        file.mv(serverPath, function (err) {
            if (err)
                return res.status(500).send(err);
            console.log(userId, postId);
            User.update(
                {_id: userId, "posts._id": postId},
                {$set: {"posts.$.imageUri": staticPath}},
                {new: true,
                upsert: true},
                function (err, user) {
                    if (err) return res.status(400).json({status: "error", message: err.message});
                    postService.getById(userId, postId, function (err, status, post) {
                        if (err) return res.status(500).json(err);
                        if (status === 404) return res.status(404).json("Post not found");
                        post.exportData(userId, function (p) {
                            res.status(200).send({status: "success", result: p});
                        })
                    });
                });
        });
    });


});

/* Report post */
router.post('/:postId/report', function (req, res) {
    // check required fields
    var userId = res.locals.userId;
    var reporterId = req.body.reporterId;
    var postId = req.params.postId;
    if (userService.userExists(reporterId, function (exists) {
            if (!exists) return res.status(404).json("Reporter not found");
            postService.report(userId, postId, reporterId, function (err, status, msg) {
                if (err) return res.status(500).json(err);
                if (status === 404) return res.status(404).send("Post not found");
                res.status(status).json(msg);
            });
        }));
});

router.all('*', function (req, res) {
    res.status(404).send("Recurso no encontrado");
});

function comparePosts(postA, postB) {
    if (postA.date < postB.date) {
        return -1;
    }
    if (postA.date > postB.date) {
        return 1;
    }
    return 0;
}