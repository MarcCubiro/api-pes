var express = require('express');
var mongoose = require('mongoose');

var User = mongoose.model('User');

module.exports.sumPoints = function(userId, points, callback) {
    User.findById(userId, function (err, user) {
        if (err) return callback(err);
        user.points += points;
        user.save(function (err, updatedUser) {
            if (err) return callback(err);
            return callback(null, 200, updatedUser);
        });
    });
};

module.exports.userExists = function(userId, callback){
    User.findOne({_id: userId}, function (err, user) {
        if(err) return callback(false);
        if(!user) return callback(false);
        return callback(true);
    })
}