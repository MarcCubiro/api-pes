var express = require('express');
var mongoose = require('mongoose');

var Event = mongoose.model('Event');

module.exports.getNearEvents = function (minDistance, maxDistance, num, geoJSONPoint, callback) {
    Event.aggregate([
        {
            $geoNear: {
                near: geoJSONPoint,
                distanceField: "distance",
                minDistance: minDistance,
                maxDistance: maxDistance,
                num: num,
                spherical: true
            }
        }
    ], function (err, result) {
        if(err) return callback(err);
        return callback(null, result);
    });
}