//var express = require('express');
var url = require('url');
var path = require('path');
var Config = require('../../config/config');
config = new Config();

module.exports.getFile = function (req, type, callback) {
    if (!req.files)
        return callback(400, 'No files were uploaded.', null, null, null);
    if (!req.files.image)
        return callback(400, 'image field is null. Image field must be called: image', null, null, null);
    var file = req.files.image;
    var origin = url.format({
        protocol: req.protocol,
        host: req.get('host')
    });
    var extension = path.extname(file.name);
    var id = req.params.id || req.params.postId;
    var fileName = id.toString() + extension;
    var serverPath = config.PATH_SERVER_IMAGES + type + "/" + fileName;
    var staticPath = origin + "/" + config.PATH_STATIC_IMAGES + type + "/" + fileName;
    return callback(null, null, file, serverPath, staticPath);
}