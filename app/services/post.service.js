var express = require('express');
var mongoose = require('mongoose');

var User = mongoose.model('User');
var Post = mongoose.model('Post');

module.exports.getById = function(userId, postId, callback) {
    User.findOne({ _id: userId }, 'posts', function(err, user) {
        if (err) return callback(err);
        var postObject = user.posts.id(postId);
        var post = new Post(postObject)
        //var post = user.posts.id(postId);
        if (postObject === null) return callback(null, 404);
        return callback(null, 200, post);
    });
};

module.exports.post = function(postRequest, callback) {
    var userId = postRequest.userId;
    User.findOne({ _id: userId }, 'posts', function(err, user) {
        if (err) return callback(err);
        var post = new Post(postRequest);
        var newLength = user.posts.push(post);
        //var post = user.posts[newLength-1];
        user.save(function(err) {
            if (err) return callback(err);
            callback(null, post);
        });
    });
};

module.exports.put = function(postRequest, callback) {
    User.update(
            {_id: postRequest.userId, "posts._id": postRequest._id},
            {$set: {"posts.$": postRequest} },
            function(err, user) {
                if (err) return callback(err);
                callback();
            });
};

module.exports.report = function(userId, postId, reporterId, callback) {
    User.findOne({ _id: userId }, 'posts', function(err, user) {
        if (err) return callback(err);
        var post = user.posts.id(postId);
        if (post === null) return callback(null, 404);
        if (post.reporters.indexOf(reporterId) > -1) return callback(null, 400, "Already reported");
        post.reporters.push(reporterId);
        user.save();
        return callback(null, 200, "Reported");
    });
};

module.exports.delete = function(userId, postId, callback) {
    User.update(
        {_id: userId},
        {$pull: {posts: {_id: postId}}},
        function(err, numAffected) {
            if (err) return callback(err);
            if (numAffected === 0) return callback({message:"Post not found"});
            callback();
        }
    );
};