var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = function() {
    var eventSchema = new Schema ({
        userId: { type: Schema.Types.ObjectId, ref: 'User', required: true },
        postId: { type: Schema.Types.ObjectId, required: true },
        title: { type: String, required: true },
        description: { type: String },
        imageUri: { type: String },
        initDate: { type: Date, default: Date.now() },
        endDate: { type: Date },
        attenders: { type: [Schema.Types.ObjectId] , ref: 'User', default: [] },
        location : {
            type: {
                type: String,
                required: true
            },
            coordinates: {
                type: [],
                required: true
            }
        }
    });

    eventSchema.methods.exportData = function(cb) {
        var userModel = this.model('User');
        var postModel = this.model('Post');
        var event = this.toObject();
        userModel.findById(event.userId, function (err, user) {
            var postObject = user.posts.id(event.postId);
            var post = new postModel(postObject);
            event.text = post.text;
            event.id = event._id;
            delete event._id;
            delete event.__v;
            return cb(event);
        })
    };

    eventSchema.methods.exportDataToPost = function (userId, postId, cb) {
        var userModel = this.model('User');
        var postModel = this.model('Post');
        var event = this.toObject();
        userModel.findById(userId, function (err, user) {
            var postObject = user.posts.id(postId);
            var post = new postModel(postObject);
            post.exportData(userId, function (post) {
                delete event.userId;
                delete event.postId;
                delete event.__v;
                post.event = event;
                return cb(post);
            });
        });
    };

    eventSchema.index({ location: '2dsphere' });
    mongoose.model('Event', eventSchema);
};