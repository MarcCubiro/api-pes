var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = function() {
    var placeSchema = new Schema ({
        type: { type: String, required: true },
        bikeStationId: {type: String, default: ""},
        availableBikes: { type: Number, default: 0 },
        orchardName: {type: String, default: ""},
        location : {
            type: {
                type: String,
                required: true
            },
            coordinates: {
                type: [],
                required: true
            }
        }
    });

    placeSchema.index({ location: '2dsphere' });
    mongoose.model('Place', placeSchema);
};