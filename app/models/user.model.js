var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt');

var Event = mongoose.model('Event');

module.exports = function () {
    var postSchema = new Schema({
        text: {type: String},
        imageUri: {type: String},
        eventId: {type: Schema.Types.ObjectId},
        date: {type: Date, default: Date.now},
        reporters: {type: [Schema.Types.ObjectId], ref: 'User', default: []}
    });

    var userSchema = new Schema({
        username: {type: String, required: true, unique: true},
        email: {type: String, required: true, unique: true},
        password: {type: String, required: true},
        name: {type: String},
        lastname: {type: String},
        points: {type: Number, default: 0},
        imageUri: {type: String},
        posts: {type: [postSchema], default: []},
        followers: {type: [Schema.Types.ObjectId], ref: 'User', default: []},
        following: {type: [Schema.Types.ObjectId], ref: 'User', default: []}
    });

    userSchema.methods.comparePassword = function (password, cb) {
        bcrypt.compare(password, this.password, function (err, isMatch) {
            if (err) return cb(err);
            cb(null, isMatch);
        });
    };

    userSchema.methods.exportData = function (cb) {
        var user = this.toObject();
        delete user["password"];
        user.id = this._id;
        delete user._id;
        if (user.posts.length === 0) return cb(user);
        newPosts = []
        user.posts.forEach(function (p, i) {
            p = new Post(p);
            p.exportDataWithoutUser(function (post) {
                newPosts.push(post);
                if (i == (user.posts.length - 1)) {
                    delete user.posts;
                    user.posts = newPosts;
                    return cb(user);
                }
            });
        });
    };

    userSchema.methods.exportDataWithOutPosts = function (cb) {
        var user = this.toObject();
        delete user["password"];
        user.id = this._id;
        delete user._id;
        delete user.posts;
        return cb(user);
    };

    postSchema.methods.exportData = function (userId, cb) {
        var eventModel = this.model('Event');
        var userModel = this.model('User');
        var post = this.toObject();
        post.id = this._id;
        delete post._id;
        userModel.findById(userId, function (err, user) {
            user.exportDataWithOutPosts(function (u) {
                post.user = u;
                if (this.eventId) {
                    eventModel.findOne(this.eventId, function (err, event) {
                        if (err || !event) return cb(post);
                        event.exportData(function (e) {
                            post.event = e;
                            return cb(post);
                        });
                    });
                }
                return cb(post);
            })
        });
    };

    postSchema.methods.exportDataWithoutUser = function (cb) {
        var post = this.toObject();
        post.id = this._id;
        delete post._id;
        this.model('Event').findOne(this._id, function (err, event) {
            if (!err && event != null) { // TODO
                event.exportData(function (e) {
                    post.event = e;
                    return cb(post);
                });
            }
            return cb(post);
        });
    };

    mongoose.model('User', userSchema);
    var Post = mongoose.model('Post', postSchema);
};