// call the packages we need
var http = require('http');
var express = require('express');
var fileUpload = require('express-fileupload');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var cors = require('cors');
var compression = require('compression');

var Config = require('./config/config');             // get config parameters
config = new Config();
require('./app/models').init();                      // init schemas IMPORTANT! before init routes!
var apiRouter = require('./app/routers/api.js');     // get api routers
//var webRouters = require('./web/routers');         // get routers

// define our app using express
var app = express();

// Connect to database
mongoose.connect(config.DB_URI, function(error, connected) {
    if (error) { console.error("Mongodb error: " + error); process.exit(1); }
    else console.log("Successfully connected to database");
});

//app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(compression());
app.use(cors());
app.use(fileUpload());
app.use(express.static('public'));

// Register our routes
app.use('/api', apiRouter);

// Start the server
if(!module.parent) { //Evitar que s'engegui un altre cop quan s'executan els tests.
    var server = http.createServer(app).listen(config.PORT, function () {
        console.log("Server running on port: " + config.PORT);
    });
}

// For testing
module.exports = {
    app: app,
    config: config
}
