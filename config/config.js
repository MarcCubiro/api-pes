if(process.env.NODE_ENV != "production")
    var winston = require('winston');

// Configuration file
module.exports = function(){
    switch(process.env.NODE_ENV){
        // production
        case 'production':
            return {
                PORT: 8080, 
                DB_URI: "mongodb://localhost/ecoboost",
                JWT_SECRET: "lkasjdñiLAHLHK2ñalsdñlkñlkjyñjkñjahsdaRSDXCAHSGC4ÑH74Ñ5LVYH2ÑHÑLHALJFHLDKSAGFKJAWFLKjksdfljhdfkjadkjfgkasgf4i25692ryefhhwH5HFBKJWVBLWF3IUR",
                STATIC_SERVER: "/static"
            }
        case 'testing':
            return {
                PORT: 3001,
                DB_URI: "mongodb://localhost/ecoboost_test",
                WINSTON_LOGGER_OPTS: {
                    transports: [
                        new winston.transports.Console({
                            colorize: true
                        })
                    ],
                    msg: "HTTP {{req.method}} {{req.url}} {{res.statusCode}} {{res.responseTime}}ms",
                    expressFormat: true,
                    meta: false,
                    colorStatus: true
                },
                JWT_SECRET: "lkasjdñiLAHLHK2ñalsdñlkñlkjyñjkñjahsdaRSDXCAHSGC4ÑH74Ñ5LVYH2ÑHÑLHALJFHLDKSAGFKJAWFLKjksdfljhdfkjadkjfgkasgf4i25692ryefhhwH5HFBKJWVBLWF3IUR",
                STATIC_SERVER: "/static",
                PATH_SERVER_IMAGES: "public/images/",
                PATH_STATIC_IMAGES: "images/",
            }
        // development 
        default:
            return {
                PORT: 3000, 
                DB_URI: "mongodb://localhost/ecoboost_dev",
                WINSTON_LOGGER_OPTS: {
                    transports: [
                        new winston.transports.Console({
                            colorize: true
                        })
                    ],
                    msg: "HTTP {{req.method}} {{req.url}} {{res.statusCode}} {{res.responseTime}}ms",
                    expressFormat: true,
                    meta: false,
                    colorStatus: true
                },
                JWT_SECRET: "lkasjdñiLAHLHK2ñalsdñlkñlkjyñjkñjahsdaRSDXCAHSGC4ÑH74Ñ5LVYH2ÑHÑLHALJFHLDKSAGFKJAWFLKjksdfljhdfkjadkjfgkasgf4i25692ryefhhwH5HFBKJWVBLWF3IUR",
                STATIC_SERVER: "/static",
                PATH_SERVER_IMAGES: "public/images/",
                PATH_STATIC_IMAGES: "images/",

            }
    }
}