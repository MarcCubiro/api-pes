process.env.NODE_ENV = 'testing';


var index = require('../index');
var app = index.app;
var config = index.config;
var request = require('supertest');
var http = require('http');
var mongoose = require('mongoose');
var User = mongoose.model('User');
var Post = mongoose.model('Post');
var Event = mongoose.model('Event');
var bcrypt = require('bcrypt');
var should = require("should");
var jwt = require('jsonwebtoken');

describe('Users', function () {
    var server;
    var user1, token1;
    var user2, token2;

    before(function(done) {
        User.remove({}, function (err) {
            user1 = new User({username:"dani",email:"dani@test.com",password: bcrypt.hashSync("pepe", 12)});
            user1.save(function () {
                token1 = jwt.sign({id:user1._id, username: user1.username}, config.JWT_SECRET);
                user2 = new User({username:"pepe",email:"pepe@test.com",password: bcrypt.hashSync("pepe", 12)});
                user2.save(function () {
                    token2 = jwt.sign({id:user2._id, username: user2.username}, config.JWT_SECRET);
                    done();
                });
            });

        });
    });

    beforeEach(function () {
        server = http.createServer(app).listen(config.PORT, function () {
            //console.log("Server running on port: " + config.PORT);
        });
    });

    afterEach(function (done) {
        server.close();
        done();
    });

    function superEscenario(cb){
        Event.remove({}, function (err) {
            var post1 = new Post({text: "Test text1"});
            var post2 = new Post({text: "Test text1"});
            var post3 = new Post({text: "Test text1"});
            user1.posts.push(post1);
            user1.posts.push(post2);
            user1.posts.push(post3);
            user1.followers.push(user2._id);
            user1.save();
            // User 2 3 posts
            user2.posts.push(post1);
            user2.posts.push(post2);
            user2.posts.push(post3);
            user2.following.push(user1._id);
            user2.save();
            cb();
        });
    }

    function rankingEscenario(){
        var stop = 10;
        for (var i = 0; i< stop ; i += 1) {
            var username = "user"+i.toString();
            var email = username+"@test.com";
            var points = i * 10;
            var tmpUser = new User({  username: username,
                email: email,
                password: bcrypt.hashSync("test", 12),
                points: points
            });
            tmpUser.save();
        }
    }


/* ----------   GET USER  ---------- */
    it('it should return user info', function (done) {
        request(server)
            .get('/api/users/'+user1._id)
            .set({ Authorization: "Bearer "+token1 })
            .end(function (err, res) {
                res.status.should.be.equal(200);
                res.body.should.have.property('username');
                res.body.should.have.property('email');
                //res.body.should.have.property('name');
                //res.body.should.have.property('lastname');
                res.body.should.have.property('following');
                res.body.should.have.property('followers');
                res.body.should.have.property('posts');
                done();
            });
    });

    it('it should not exists user', function (done) {
        request(server)
            .get('/api/users/'+9999)
            .set({ Authorization: "Bearer "+token1 })
            .end(function (err, res) {
                res.status.should.be.equal(404);
                res.body.should.be.equal("User not found.");
                done();
            });
    });


/* ----------   EDIT   ---------- */
// success
    it('should edit user', function (done) {
        request(server)
            .put('/api/users/'+user1._id)
            .set({ Authorization: "Bearer "+token1 })
            .send({
                "username": "userNameTest2",
                "password": "testPass2",
                "email" : "testemail2@gmail.com",
                "name" : "pepe",
                "lastname" : "criado"
            })
            .expect(200, done);
    });

// error same username
    it('should not edit user', function (done) {
        request(server)
            .put('/api/users/'+user1._id)
            .set({ Authorization: "Bearer "+token1 })
            .send({
                "username": "pepe"
            })
            .expect(400, done);
    });

    it('should follow user', function (done) {
        request(server)
            .post('/api/users/'+user1._id+'/follow')
            .set({ Authorization: "Bearer "+token1 })
            .send({
                userTofollowId : user2._id
            })
            .end(function (err, res) {
                res.status.should.be.equal(200);
                res.body.should.be.equal("done");
                done();
            });
    });

    it('should return feed correctly', function (done) {
        superEscenario(function () {
            request(server)
                .get('/api/users/'+user2._id+'/feed')
                .set({ Authorization: "Bearer "+token2 })
                .end(function (err, res) {
                    res.status.should.be.equal(200);
                    res.body.should.have.property('posts');
                    res.body.posts.length.should.be.equal(6);//TODO: a vegades dona menys
                    done();
                });
        });
    });

    it('should return feed correctly with events', function testSlash(done) {
        superEscenario(function () {
            request(server)
                .get('/api/users/'+user2._id+'/feed?longitude=41.338583&latitude=2.192757')
                .set({ Authorization: "Bearer "+token2 })
                .end(function (err, res) {
                    res.status.should.be.equal(200);
                    res.body.should.have.property('posts');
                    res.body.posts.length.should.be.equal(6);//TODO: Quan executes tots els tests, dona mes de 6.
                    done();
                });
        });
    });

    it('should return error 401', function (done) {
        request(server)
            .get('/api/users/333/feed')
            .set({ Authorization: "Bearer "+token1 })
            .end(function (err, res) {
                res.status.should.be.equal(401);
                done();
            });
    });

    it('should return top 5 ranking', function (done) {
        this.timeout(5000);
        rankingEscenario();
        request(server)
            .get('/api/users/ranking')
            .set({ Authorization: "Bearer "+token1 })
            .end(function (err, res) {
                res.status.should.be.equal(200);
                res.body.should.have.property("users");
                res.body.users.length.should.be.lessThan(6);
                done();
            });
    });


});
