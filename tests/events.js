/**
 * Created by dani on 17/03/17.
 */

process.env.NODE_ENV = 'testing';


var index = require('../index');
var app = index.app;
var config = index.config;
var request = require('supertest');
var http = require('http');
var mongoose = require('mongoose');
var User = mongoose.model('User');
var Event = mongoose.model('Event');
var bcrypt = require('bcrypt');
// var supertest = require("supertest");
var should = require("should");
var jwt = require('jsonwebtoken');
//
// // This agent refers to PORT where program is runninng.
//
// var server = supertest.agent("http://localhost:3000");
describe('Events', function () {
    var server;
    var user;
    var event;
    var token;

    before(function (done) {
        User.remove({}, function () {
            Event.remove({}, function () {
                var password = 'test';
                var hash = bcrypt.hashSync(password, 12);
                user = new User({username: "user", email: "user@test.com", password: hash});
                token = jwt.sign({id:user._id, username: user.username}, config.JWT_SECRET);
                user.save(function () {
                    done();
                });
            });
        });
    });

    beforeEach(function (done) {
        server = http.createServer(app).listen(config.PORT, function () {
            //console.log("Server running on port: " + config.PORT);
            scenario1(done);
        });

    });


    afterEach(function (done) {
        server.close();
        done();
    });

    function scenario1(done){
        user.posts = [];
        var post = {
            //_id: mongoose.Types.ObjectId(),
            text: "Quedat tranquil Pau"
        };
        user.posts.push(post);
        user.save(function (err, user) {
            post = user.posts[0];
            event = new Event({
                userId: user._id,
                postId: post._id,
                title: "title",
                location:{
                    "type": "Point",
                    "coordinates": [41.338583, 2.192757]
                }
            });
            event.save(function (err, eventSaved) {
                event = eventSaved;
                done();
            });
        });
    }

    it('it should get all events correctly', function (done) {
        request(server)
            .get('/api/events')
            .end(function (err, res) {
                res.status.should.be.equal(200);
                res.body.events.length.should.be.equal(1);
                done();
            });
    });

    it('it should get event correctly', function (done) {
        request(server)
            .get('/api/events/'+event._id)
            .set({ Authorization: "Bearer "+token })
            .end(function (err, res) {
                res.status.should.be.equal(200);
                res.body.should.have.property('userId');
                res.body.userId.should.be.equal(user._id.toString());
                res.body.should.have.property('postId');
                res.body.postId.should.be.equal(event.postId.toString());
                res.body.should.have.property('title');
                res.body.title.should.be.equal("title");
                done();
            });
    });

    it('it should create event correctly', function (done) {
        request(server)
            .post('/api/events')
            .set({ Authorization: "Bearer "+token })
            .send({post: {  userId: user._id,
                            text:"Text de prova de post"},
                    event:
                        {
                            userId: user._id,
                            title:"Title de prova de test",
                            description: "Descripcio de prova",
                            location:{
                                "type": "Point",
                                "coordinates": [41.338583, 2.192757]
                            }
                        }
                    })
            .end(function (err, res) {
                res.status.should.be.equal(201);
                res.body.should.have.property('event');
                res.body.should.have.property('post');
                done();
            });
    });

    it('it should update event correctly', function (done) {
        request(server)
            .put('/api/events/'+event._id)
            .set({ Authorization: "Bearer "+token })
            .send({title:"Title actualitzat", userId:user._id})
            .end(function (err, res) {
                res.status.should.be.equal(200);
                res.body.should.have.property('title');
                res.body.title.should.be.equal('Title actualitzat');
                done();
            });
    });

    it('it should delete event correctly', function (done) {
        var post = {
            userId: user._id,
            text: "Nou post"
        };
        user.posts.push(post);
        post = user.posts[0];
        user.save(function (err, user) {
            post = user.posts[0];
            var e = new Event({userId: user._id, postId: post._id, title: "title", location:{
                "type": "Point",
                "coordinates": [41.338583, 2.192757]
            }});
            e.save(function (err, event) {
                request(server)
                    .delete('/api/events/'+e._id)
                    .set({ Authorization: "Bearer "+token })
                    .send({userId:user._id})
                    .end(function (err, res) {
                        res.status.should.be.equal(200);
                        res.text.should.be.equal('Event removed');
                        done();
                    });
            });
        });
    });

    it('it should enroll correctly', function (done) {
        request(server)
            .post('/api/events/' + event._id + '/enroll')
            .set({ Authorization: "Bearer "+token })
            .send({userId: user._id})
            .end(function (err, res) {
                res.status.should.be.equal(200);
                res.body.should.be.equal('Enrolled');
                done();
            });
    });

    it('it should already enrolled', function (done) {
        event.attenders.push(user._id);
        event.save(function () {
            request(server)
                .post('/api/events/' + event._id + '/enroll')
                .set({ Authorization: "Bearer "+token })
                .send({userId: user._id})
                .end(function (err, res) {
                    res.status.should.be.equal(400);
                    res.body.should.be.equal('Already enrolled');
                    done();
                });
        })

    });

    it('it should unattend correctly', function (done) {
        event.attenders.push(user._id);
        event.save(function () {
            request(server)
                .post('/api/events/' + event._id + '/unattend')
                .set({ Authorization: "Bearer "+token })
                .send({userId: user._id})
                .end(function (err, res) {
                    res.status.should.be.equal(200);
                    res.body.should.be.equal('Not attending');
                    done();
                });
        });
    });

    it('it should not enrolled yet', function (done) {
        event.attenders.remove(user._id);
        event.save(function () {
            request(server)
                .post('/api/events/' + event._id + '/unattend')
                .set({ Authorization: "Bearer "+token })
                .send({userId: user._id})
                .end(function (err, res) {
                    res.status.should.be.equal(400);
                    res.body.should.be.equal('Not enrolled');
                    done();
                });
        });
    });

    it('it should report correctly', function (done) {
        request(server)
            .post('/api/events/' + event._id + '/report')
            .set({ Authorization: "Bearer "+token })
            .send({userId: user._id})
            .end(function (err, res) {
                res.status.should.be.equal(200);
                res.body.should.be.equal('Reported');
                done();
            });
    });

    it('it should already reported', function (done) {
        /*Simulacio de reportar.*/
        user.posts.forEach(function (post) {
            if (post._id === event.postId) {
                post.reporters.push(user._id);
                user.save(function () {
                    request(server)
                        .post('/api/events/' + event._id + '/report')
                        .set({ Authorization: "Bearer "+token })
                        .send({userId: user._id})
                        .end(function (err, res) {
                            res.status.should.be.equal(400);
                            res.body.should.be.equal('Already reported');
                            done();
                        });
                });
            }
        });

    });
});
