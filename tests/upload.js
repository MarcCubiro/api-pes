/**
 * Created by dani on 17/03/17.
 */

process.env.NODE_ENV = 'testing';

var index = require('../index');
var app = index.app;
var config = index.config;
var request = require('supertest');
var http = require('http');
var mongoose = require('mongoose');
var User = mongoose.model('User');
var Post = mongoose.model('Post');
var Event = mongoose.model('Event');
var constants = require('../app/utils/constants');
var bcrypt = require('bcrypt');
var should = require("should");
var fs = require("fs");
var jwt = require('jsonwebtoken');

describe('Upload', function () {
    var server;
    var user;
    var token;
    var filename = 'tests/test.jpg';
    var post;
    var event;

    before(function (done) {
        User.remove({}, function (err) {
            var password = 'test';
            var hash = bcrypt.hashSync(password, 12);
            user = new User({username:"dani",email:"dani@test.com",password:hash});
            user.save(function () {
                token = jwt.sign({id:user._id, username: user.username}, config.JWT_SECRET);
                done();
            });
        });
    });

    beforeEach(function (done) {
        server = http.createServer(app).listen(config.PORT, function () {
            scenario1(done);
            //done();
        });

    });


    afterEach(function (done) {
        server.close();
        done();
    });

    // scenario with 1 user and 1 post and 1 event
    function scenario1(done){
        user.posts = [];
        post = new Post({text: "Test upload image"});
        user.posts.push(post);
        user.save(function (err, user) {
            post = user.posts[0];
            event = new Event({
                userId: user._id,
                postId: post._id,
                title: "Event del test de upload image",
                location:{
                    "type": "Point",
                    "coordinates": [41.338583, 2.192757]
                }
            });
            event.save(function (err, eventSaved) {
                event = eventSaved;
                done();
            });
        });
    }



    it('it should upload user image correctly', function (done) {
        request(server)
            .post('/api/users/'+user._id+"/image")
            .set({ Authorization: "Bearer "+token })
            .attach('image', filename)
            .end(function (err, res) {
                res.status.should.be.equal(200);
                res.body.should.have.property("status");
                res.body.should.have.property("result");
                res.body.result.should.have.property("imageUri");
                done();
                });
    });

    it('it should upload post image correctly', function (done) {
        request(server)
            .post('/api/users/'+user._id+"/posts/"+post._id+"/image")
            .set({ Authorization: "Bearer "+token })
            .attach('image', filename)
            .end(function (err, res) {
                res.status.should.be.equal(200);
                res.body.should.have.property("status");
                res.body.should.have.property("result");
                res.body.result.should.have.property("imageUri");
                done();
            });
    });
    it('it should upload event image correctly', function (done) {
        request(server)
            .post('/api/events/'+event._id+"/image")
            .set({ Authorization: "Bearer "+token })
            .attach('image', filename)
            .field('userId', user._id)
            .end(function (err, res) {
                res.status.should.be.equal(200);
                res.body.should.have.property("status");
                res.body.should.have.property("result");
                res.body.result.should.have.property("imageUri");
                done();
            });
    });
});
