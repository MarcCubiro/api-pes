process.env.NODE_ENV = 'testing';

var index = require('../index');
var app = index.app;
var config = index.config;
var request = require('supertest');
var http = require('http');
var mongoose = require('mongoose');
var User = mongoose.model('User');
var Post = mongoose.model('Post');
var bcrypt = require('bcrypt');
var should = require("should");
var jwt = require('jsonwebtoken');

describe('Posts', function () {
    var server;
    var user;
    var post;
    var base;
    var token;

    before(function(done) {
        User.remove({}, function () {
            var password = 'test';
            var hash = bcrypt.hashSync(password, 12);
            user = new User({username: "user", email: "user@test.com", password: hash});
            user.save(function () {
                base = "/api/users/"+user._id+"/posts/";
                token = jwt.sign({id:user._id, username: user.username}, config.JWT_SECRET);
                done();
            });
        });
    });

    beforeEach(function (done) {
        server = http.createServer(app).listen(config.PORT, function () {
            //console.log("Server running on port: " + config.PORT);
            scenario1(done);
        });
    });

    afterEach(function (done) {
        server.close();
        done();
    });

    // scenario with 1 user and 1 post
    function scenario1(done){
        user.posts = [];
        post = new Post({text: "Test text"});
        user.posts.push(post);
        user.save(function () {
            done();
        });
    }

    it('it should return the post by user id', function (done) {
        request(server)
            .get(base+"/")
            .set({ Authorization: "Bearer "+token })
            .end(function (err, res) {
                console.log(res.body);
                res.status.should.be.equal(200);
                res.body.should.have.property("posts");
                done();
            });
    });

    it("it should return an error of unauthorized", function (done) {
        request(server)
            .get("/api/users/123/posts/")
            .set({ Authorization: "Bearer "+token })
            .end(function (err, res) {
                res.status.should.be.equal(401);
                done();
            });
    });

    it('it should return the post by id', function (done) {
        request(server)
            .get(base+"/"+post._id)
            .set({ Authorization: "Bearer "+token })
            .end(function (err, res) {
                res.status.should.be.equal(200);
                res.body.should.have.property('text').equal('Test text');
                done();
            });
    });

    it('it should return an error 404', function (done) {
        request(server)
            .get(base+"/1234")
            .set({ Authorization: "Bearer "+token })
            .end(function (err, res) {
                res.status.should.be.equal(404);
                res.body.should.be.equal('Post not found');
                done();
            });
    });

    it('it should create post correctly', function (done) {
        request(server)
            .post(base+'/')
            .set({ Authorization: "Bearer "+token })
            .send({
                text:"Text de prova insert"
            })
            .end(function (err, res) {
                res.status.should.be.equal(201);
                res.body.should.have.property('text').equal('Text de prova insert');
                done();
            });
    });

    it('it should not create new post of other user', function (done) {
        request(server)
            .post("/api/users/123/posts/")
            .set({ Authorization: "Bearer "+token })
            .send({
                text:"Text de prova insert"
            })
            .end(function (err, res) {
                res.status.should.be.equal(401);
                done();
            });
    });

    it('it should update post correctly', function (done) {
        request(server)
            .put(base+'/'+post._id)
            .set({ Authorization: "Bearer "+token })
            .send({
                text:"Text de prova update"
            })
            .end(function (err, res) {
                res.status.should.be.equal(200);
                res.body.should.be.equal('Post updated');
                done();
            });
    });

    it('it should not update post of other user', function testSlash(done) {
        request(server)
            .put("/api/users/123/posts/"+post._id)
            .set({ Authorization: "Bearer "+token })
            .send({
                text:"Text de prova insert"
            })
            .end(function (err, res) {
                res.status.should.be.equal(401);
                done();
            });
    });

    it('it should delete post correctly', function (done) {
        request(server)
            .delete(base+'/'+post._id)
            .set({ Authorization: "Bearer "+token })
            .end(function (err, res) {
                res.status.should.be.equal(200);
                res.body.should.be.equal('Post deleted');
                done();
            });
    });

    it('it should not delete post of another', function testSlash(done) {
        request(server)
            .delete("/api/users/123/posts/"+post._id)
            .set({ Authorization: "Bearer "+token })
            .end(function (err, res) {
                res.status.should.be.equal(401);
                done();
            });
    });

    it('should not delete and return an error 404', function testSlash(done) {
        request(server)
            .delete(base+'/1234')
            .set({ Authorization: "Bearer "+token })
            .end(function (err, res) {
                res.status.should.be.equal(404);
                done();
            });
    });

    it('should report post correctly', function testSlash(done) {
        request(server)
            .post(base+post._id+'/report')
            .set({ Authorization: "Bearer "+token })
            .send({
                reporterId: user._id
            })
            .end(function (err, res) {
                res.status.should.be.equal(200);
                res.body.should.be.equal('Reported');
                done();
            });
    });

    it('should response 404 reporter not found', function testSlash(done) {
        request(server)
            .post(base+post._id+'/report')
            .set({ Authorization: "Bearer "+token })
            .send({
                reporterId:"122"
            })
            .end(function (err, res) {
                res.status.should.be.equal(404);
                res.body.should.be.equal('Reporter not found');
                done();
            });
    });

    it('should response 400 post already reported', function testSlash(done) {
        request(server)
            .post(base+post._id+'/report')
            .set({ Authorization: "Bearer "+token })
            .send({
                reporterId: user._id
            })
            .end(function () {
                request(server)
                    .post(base+post._id+'/report')
                    .set({ Authorization: "Bearer "+token })
                    .send({
                        reporterId: user._id
                    })
                    .end(function (err, res) {
                        res.status.should.be.equal(400);
                        res.body.should.be.equal('Already reported');
                        done();
                    });
            });
    });

});
