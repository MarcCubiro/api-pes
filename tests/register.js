/**
 * Created by dani on 17/03/17.
 */

process.env.NODE_ENV = 'testing';


var index = require('../index');
var app = index.app;
var config = index.config;
var request = require('supertest');
var http = require('http');
var mongoose = require('mongoose');
var User = mongoose.model('User');
var constants = require('../app/utils/constants');
var bcrypt = require('bcrypt');
// var supertest = require("supertest");
var should = require("should");
//
// // This agent refers to PORT where program is runninng.
//
// var server = supertest.agent("http://localhost:3000");
describe('Register', function () {
    var server;

    before(function(done) {
        User.remove({}, function (err) {
            done();
        });
    });

    beforeEach(function () {
        server = http.createServer(app).listen(config.PORT, function () {
            //console.log("Server running on port: " + config.PORT);
        });

    });


    afterEach(function (done) {
        server.close();
        done();
    });

    it('it should register correctly', function (done) {
        request(server)
            .post('/api/users')
            .send({ username: "user", email: "user@test.com", password: "test" })
            .end(function (err, res) {
                res.status.should.be.equal(201);
                res.body.should.have.property('token');
                res.body.should.have.property('user');
                res.body.user.should.have.property('points');
                res.body.user.points.should.be.equal(constants.REGISTER_POINTS);
                done();
            });
    });



    /* WARNING:  duplicated tests?*/
    /* ----------   REGISTER   ---------- */
    /*
// success
    it('should register new user', function (done) {
        request(server)
            .post('/api/users/')
            .send({
                "username": "userNameTest",
                "password": "testPass",
                "email" : "testemail@gmail.com"
            })
            .expect(201, done)

    });
// error: wrong params
    it('should not register a new user and return status 400', function testSlash(done) {
        request(server)
            .post('/api/users/')
            .send({
                "username": "userNameTest",
                "password": "testPass"
            })
            .expect(400, {
                status : "Username, email or password are empty"
            }, done)
    });*/

});
