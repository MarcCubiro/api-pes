/**
 * Created by dani on 17/03/17.
 */

process.env.NODE_ENV = 'testing';


var index = require('../index');
var app = index.app;
var config = index.config;
var request = require('supertest');
var http = require('http');
var mongoose = require('mongoose');
var User = mongoose.model('User');
var bcrypt = require('bcrypt');
// var supertest = require("supertest");
var should = require("should");
//
// // This agent refers to PORT where program is runninng.
//
// var server = supertest.agent("http://localhost:3000");
describe('Login', function () {
    var server;

    before(function(done) {
        User.remove({}, function (err) {
            var password = 'test';
            var hash = bcrypt.hashSync(password, 12);
            var user = new User({username:"dani",email:"dani@test.com",password:hash});
            user.save();
            done();
        });
    });

    beforeEach(function () {
        server = http.createServer(app).listen(config.PORT, function () {
            //console.log("Server running on port: " + config.PORT);
        });

    });


    afterEach(function (done) {
        server.close();
        done();
    });

    it('it should login correctly', function (done) {
        request(server)
            .post('/api/login')
            .send({username:"dani", password:"test"})
            .end(function (err, res) {
                res.status.should.be.equal(200);
                res.body.should.have.property('token');
                res.body.should.have.property('user');
                res.body.user.should.have.property('id');
                done();
            });
    });

    it('it should not login, user not exists', function (done) {
        request(server)
            .post('/api/login')
            .send({username:"inventado", password:"test"})
            .expect(404, done);

    });

    it('it should not login, password is incorrect', function (done) {
        request(server)
            .post('/api/login')
            .send({username:"dani", password:"inventado"})
            .expect(401, done);
    });
});
